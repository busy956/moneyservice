package com.bj.moneydiaryapi.model;

import com.bj.moneydiaryapi.enums.MoneyBreakDown;
import com.bj.moneydiaryapi.enums.MoneyPayment;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MoneyRequest {
    private String moneyContent;
    private Long money;

    @Enumerated(value = EnumType.STRING)
    private MoneyPayment moneyPayment;

    private String etcMemo;

    @Enumerated(value = EnumType.STRING)
    private MoneyBreakDown moneyBreakDown;
}

package com.bj.moneydiaryapi.model;

import com.bj.moneydiaryapi.enums.MoneyBreakDown;
import com.bj.moneydiaryapi.enums.MoneyPayment;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MoneyItem {
    private Long id;
    private LocalDate dateCreate;
    private String moneyContent;
    private Long money;
    private String moneyPayment;
    private String moneyBreakDown;
    private Boolean isCard;
}

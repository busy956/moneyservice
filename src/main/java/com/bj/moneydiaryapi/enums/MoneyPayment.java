package com.bj.moneydiaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyPayment {
    CASH("현금", false),
    CARD("카드", true);

    private final String payMethod;
    private final Boolean isCard;
}

package com.bj.moneydiaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyBreakDown {
    INCOME("수입"),
    EXPENSE("지출");

    private final String details;
}

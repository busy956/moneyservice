package com.bj.moneydiaryapi.repository;

import com.bj.moneydiaryapi.entity.Money;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyRepository extends JpaRepository<Money, Long> {
}

package com.bj.moneydiaryapi.service;

import com.bj.moneydiaryapi.entity.Money;
import com.bj.moneydiaryapi.model.MoneyItem;
import com.bj.moneydiaryapi.model.MoneyRequest;
import com.bj.moneydiaryapi.repository.MoneyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MoneyService {
    private final MoneyRepository moneyRepository;

    public void setMoney(MoneyRequest request) {
        Money addDate = new Money();
        addDate.setDateCreate(LocalDate.now());
        addDate.setMoneyContent(request.getMoneyContent());
        addDate.setMoney(request.getMoney());
        addDate.setMoneyPayment(request.getMoneyPayment());
        addDate.setEtcMemo(request.getEtcMemo());
        addDate.setMoneyBreakDown(request.getMoneyBreakDown());

        moneyRepository.save(addDate);
    }

    public List<MoneyItem> getMoneys() {
        List<Money> originList = moneyRepository.findAll();

        List<MoneyItem> result = new LinkedList<>();

        for (Money money : originList) {
            MoneyItem addItem = new MoneyItem();
            addItem.setId(money.getId());
            addItem.setDateCreate(money.getDateCreate());
            addItem.setMoneyContent(money.getMoneyContent());
            addItem.setMoney(money.getMoney());
            addItem.setMoneyPayment(money.getMoneyPayment().getPayMethod());
            addItem.setMoneyBreakDown(money.getMoneyBreakDown().getDetails());
            addItem.setIsCard(money.getMoneyPayment().getIsCard());

            result.add(addItem);
        }

        return result;
    }
}
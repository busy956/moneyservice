package com.bj.moneydiaryapi.controller;

import com.bj.moneydiaryapi.model.MoneyItem;
import com.bj.moneydiaryapi.model.MoneyRequest;
import com.bj.moneydiaryapi.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/money")
public class MoneyController {
    private final MoneyService moneyService;

    @PostMapping("/my")
    public String setMoney(@RequestBody MoneyRequest request) {
        moneyService.setMoney(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MoneyItem> getMoneys() {
        return moneyService.getMoneys();
    }
}

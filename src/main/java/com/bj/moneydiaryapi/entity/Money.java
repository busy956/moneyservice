package com.bj.moneydiaryapi.entity;

import com.bj.moneydiaryapi.enums.MoneyBreakDown;
import com.bj.moneydiaryapi.enums.MoneyPayment;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Money {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 40)
    private String moneyContent;

    @Column(nullable = false)
    private Long money;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private MoneyPayment moneyPayment;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private MoneyBreakDown moneyBreakDown;
}
